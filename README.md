# GovPay payment proxy
Dir con i doc nostri: https://drive.google.com/drive/u/1/folders/1Xy061XAVLJmqaifele8sDBgq9i5FyMak

## Cosa fa
Questo servizio si occupa di fare da intermediario tra la stanza del cittadino e il provider di pagamento GovPay

## Stadio di sviluppo
Il servizio è nello stadio di staging


## Come si usa
Da terminale:
1. `git clone https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy.git`
2. `cd govpay-payment-proxy`
3. `docker-compose up -d` (assicurarsi che kafka sia su, in caso contrario digitare `docker-compose up -d kafka`)
4. Accedere alla documentazione delle API via `http://0.0.0.0:8000/docs`
5. Usare la `POST /tenants` e inserire la configurazione usando lo schema indicato
6. Usare la `POST /services` e inserire la configurazione usando lo schema indicato (inserire come tenant_id l'id del tenant precedentemente creato)
7. Accedere a `kafka-ui` via `http://localhost:8080`
8. Accedere al topic `payments` e andare nella sezione Messages
9. Copiare il messaggio presente nel topic
10. Premere "Produce Message",
11. Copiare il messaggio settando
    1. Il campo `id` ad un uuid valido
    2. Il campo `iud` all esadecimale dell'id dello step precedente (basta eliminare i trattini)
    3. Il campo `expire_at` ad una data successiva a quella odierna
    4. Il campo `tenant_id` all'id settato nello step 5
    5. Il campo `service_id` all'id settato nello step 6
12. Inviare il messaggio
13. Accedere alla documentazione delle API via `http://0.0.0.0:8000/docs
14. Usare la GET `/online-payment/{payment_id}` e inserire l'id settato allo step 11.1
15. Usare il link restituito per pagare


## Configurazione
### Configurazione variabili d'ambiente
| Nome                    | Default                                  | Descrizione                                                                                                        |
|-------------------------|------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| CANCEL_PAYMENT          | false                                    | Se attivata il proxy annullerà tutti i pagamenti di cui viene richiesto l'aggiornamento ma che non vengono trovati |
| KAFKA_TOPIC_NAME        | payments                                 | Topic di kafka da cui si leggono i pagamenti da processare                                                         |
| KAFKA_BOOTSTRAP_SERVERS | kafka:9092                               | Endpoint di kafka                                                                                                  |
| KAFKA_GROUP_ID          | govpay_payment_proxy                     | Nome del consumer group                                                                                            |
| GOTENBERG_URL           | http://0.0.0.0:3000/convert/html         | Url per generare il pdf della ricevuta di pagamento                                                                |
| BASE_PATH_CONFIG        | tenants/                                 | Path di salvataggio delle configurazioni di tenant e servizio                                                      |
| BASE_PATH_EVENT         | payments/                                | Path di salvataggio dei pagamenti                                                                                  |
| STORAGE_TYPE            | MINIO                                    | Tipo di storage dei pagamenti                                                                                      |
| STORAGE_KEY             | AKIAIOSFODNN7EXAMPLE                     | Chiave dello storage dei pagamenti e delle configurazioni di tenant e servizio                                     |
| STORAGE_SECRET          | wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY | Password dello storage dei pagamenti e delle configurazioni di tenant e servizio                                   |
| STORAGE_BUCKET_NAME     | payments                                 | Nome dello storage dei pagamenti e delle configurazioni di tenant e servizio                                       |
| STORAGE_ENDPOINT        | minio:9000                               | Endpoint dello storage dei pagamenti e della configurazioni di tenanto e servizio                                  |

### Configurazione Tenant
```json
{
  "tax_identification_number": "1234567891",
  "code": "CBugliano", "sil_id": "SIL_CBUGLIANO_OPEN",
  "password": "76JWL*G6bS!OM2e9*3Fg",
  "cert": "-----BEGIN CERTIFICATE REQUEST-----MIIB9TCCAWACAQAwgbgxGTAXBgNVBAoMEFF1b1ZhZGlzIExpbWl0ZWQxHDAaBgNVBAsME0RvY3VtZW50IERlcGFydG1lbnQxOTA3BgNVBAMMMFdoeSBhcmUgeW91IGRl-----END CERTIFICATE REQUEST-----",
  "application_code": "",
  "active": false,
  "id": "b212c4b4-db26-4404-8c7c-47dab99dd2e6"
}
```
### Configurazione Servizio
```json
{
  "tenant_id": "b212c4b4-db26-4404-8c7c-47dab99dd2e6",
  "code": "CONTR_PRTPM",
  "description": "Abbonamento aree di sosta",
  "split": [
    {
      "split_id": "c_1",
      "split_type": "Tipo c1",
      "split_code": "Codice c1",
      "split_description": "Descrizione c1",
      "split_amount": 0.5,
      "split_budget_chapter": "Capitolo di bilancio c1",
      "split_assessment": "Accertamento c1"
    },
    {
      "split_id": "c_2",
      "split_type": "Tipo c2",
      "split_code": "Codice c2",
      "split_description": "Descrizione c2",
      "split_amount": 0.5,
      "split_budget_chapter": "Capitolo di bilancio c2",
      "split_assessment": "Accertamento c2"
    }
  ],
  "active": true,
  "id": "d8e7e40e-9b2f-456a-ba50-5f460efbfd1c"
}
```
### Configurazione Endpoint


IP

- 93.41.234.251 (italia)
- 78.47.94.152 (germania)
- 54.220.150.231 (irlanda)
- 63.33.155.248 (irlanda)

## Sast

## Dependency Scanning

## Renovate

## Coding Style

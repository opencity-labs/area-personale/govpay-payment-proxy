#!/usr/bin/env python
import json
import os
from contextlib import asynccontextmanager
from uuid import UUID

import uvicorn as uvicorn
from dotenv import load_dotenv
from fastapi import Body, FastAPI, Path, Request, Response, status
from fastapi.exceptions import RequestValidationError
from fastapi.openapi.utils import get_openapi
from fastapi.responses import JSONResponse, RedirectResponse
from pydantic import BaseModel, ValidationError
from starlette_exporter import PrometheusMiddleware, handle_metrics

from __init__ import __version__ as app_version
from configurator import (
    ConfigurationNotAllowed,
    ConfigurationNotFound,
    SchemaNotFound,
    Service,
    ServiceConfigurator,
    ServiceUpdate,
    Tenant,
    TenantConfigurator,
    TenantUpdate,
    get_service_schema,
    get_tenant_schema,
)
from errors import ERR_422_SCHEMA, ErrorMessage, JSONResponseCustom, error_message
from govpay import GovPay, GovPayException
from logger import proxy_logger as logger
from monitoring import inc_api_metrics
from mykafka import Kafka

load_dotenv()

logger.info('Starting')
APP_HOST = os.environ.get('APP_HOST', '0.0.0.0')
APP_PORT = os.environ.get('APP_PORT', 8000)
EXTERNAL_API_URL = os.environ.get('EXTERNAL_API_URL', 'http://0.0.0.0:8000')
INTERNAL_API_URL = os.environ.get('INTERNAL_API_URL', 'http://0.0.0.0:8000')
PROMETHEUS_PREFIX = os.getenv('PROMETHEUS_JOB_NAME', default='govpay_payment_proxy')
GOTENBERG_URL = os.environ.get('GOTENBERG_URL', 'http://0.0.0.0:3000/convert/html')
GOVPAY_CONFIG_PATH = os.environ.get('GOVPAY_CONFIG_PATH', '../tenants')
KAFKA_TOPIC_NAME = os.environ.get('KAFKA_TOPIC_NAME', 'payments')
KAFKA_BOOTSTRAP_SERVERS = os.environ.get('KAFKA_BOOTSTRAP_SERVERS', 'localhost:29092').split(',')
KAFKA_GROUP_ID = os.environ.get('KAFKA_GROUP_ID', 'govpay_payment_proxy')
FASTAPI_BASE_URL = os.getenv('FASTAPI_BASE_URL', '')
ENV = os.getenv('ENV', 'LOCAL')
APP_NAME = 'govpay-payment-proxy'
PAGOPA_PAYMENT_URL = os.environ.get('PAGOPA_PAYMENT_URL')

kafka = Kafka(
    topic=KAFKA_TOPIC_NAME,
    bootstrap_servers=KAFKA_BOOTSTRAP_SERVERS,
    group_id=KAFKA_GROUP_ID,
    enable_auto_commit=True,
    seek_to_beginning=False,
    app_id=f'govpay-payment-proxy:{app_version}',
)


@asynccontextmanager
async def manage_startup_and_shutdown(app: FastAPI):
    logger.info('Initializing API ...')

    await kafka.initialize()
    await kafka.consume(govpay.process_payment)

    yield

    logger.info('Stopping consumer')
    logger.info('Stopping application')

    kafka.consumer_task.cancel()
    await kafka.consumer.stop()


# instantiate the API
app = FastAPI(
    openapi_url=f'{FASTAPI_BASE_URL}/openapi.json',
    docs_url=f'{FASTAPI_BASE_URL}/docs',
    lifespan=manage_startup_and_shutdown,
    openapi_tags=[{'name': 'Status'}, {'name': 'Payments'}, {'name': 'Tenants'}, {'name': 'Services'}],
    responses={422: ERR_422_SCHEMA},
)

app.add_middleware(PrometheusMiddleware, app_name=APP_NAME, prefix=PROMETHEUS_PREFIX)
app.add_route('/metrics', handle_metrics)


class Status(BaseModel):
    status: str


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    await inc_api_metrics(env=ENV, method=request.method, status_code=422)
    return error_message(
        err_type='/errors/request-validation-error',
        title='Request Validation Error',
        status=422,
        detail=exc.__dict__,
        instance=None,
    )


@app.get(
    '/status',
    tags=['Status'],
    description='Get Service Status',
    status_code=200,
    response_class=JSONResponseCustom,
    responses={
        503: {'model': ErrorMessage, 'description': 'Service Unavailable'},
        200: {'model': Status, 'description': 'Successful Response'},
    },
)
async def get_status() -> JSONResponse:
    try:
        await inc_api_metrics(env=ENV, method='GET', status_code=200)
        return JSONResponse({'status': 'Everything OK!'}, media_type='application/problem+json')
    except Exception as exc:
        await inc_api_metrics(env=ENV, method='GET', status_code=503)
        return error_message('/errors/service-unavailable', 'Service Unavailable', 503, str(exc), None)


@app.get('/online-payment/{payment_id}', description='Get Online Url', tags=['Payments'])
async def get_online_url(payment_id: str = Path(example='5f99646d-769e-4d73-ba28-cffd30a77e74')):  # noqa: B008
    try:
        payment_url, payment = await govpay.get_online_url(payment_id=payment_id)
        logger.debug(payment_url)
        await kafka.send_event(payment)
        await inc_api_metrics(env=ENV, method='GET', status_code=307)
        return RedirectResponse(payment_url)
    except GovPayException as gpe:
        await inc_api_metrics(env=ENV, method='GET', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(gpe), None)


@app.get('/notice/{payment_id}', description='Get Notice File', tags=['Payments'])
async def get_notice_file(payment_id: str = Path(example='5f99646d-769e-4d73-ba28-cffd30a77e74')):  # noqa: B008
    try:
        notice_pdf, payment = await govpay.get_notice_file(payment_id=payment_id)
        await kafka.send_event(payment)
        await inc_api_metrics(env=ENV, method='GET', status_code=200)
        return Response(notice_pdf, media_type='application/pdf')
    except GovPayException as gpe:
        await inc_api_metrics(env=ENV, method='GET', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(gpe), None)


@app.get('/receipt/{payment_id}', description='Get Receipt File', tags=['Payments'])
async def get_receipt_file(payment_id: str = Path(example='5f99646d-769e-4d73-ba28-cffd30a77e74')):  # noqa: B008
    try:
        receipt_pdf, payment = await govpay.get_receipt_file(payment_id=payment_id)
        # await kafka.send_event(payment)
        await inc_api_metrics(env=ENV, method='GET', status_code=200)
        return Response(receipt_pdf, media_type='application/pdf')
    except GovPayException as gpe:
        await inc_api_metrics(env=ENV, method='GET', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(gpe), None)


@app.get('/update/{payment_id}', description='Check and Update Payment Status', tags=['Payments'])
async def check_for_updates(payment_id: str = Path(example='5f99646d-769e-4d73-ba28-cffd30a77e74')):  # noqa: B008
    try:
        payment, status_changed = await govpay.check_for_updates(payment_id=payment_id)
        logger.debug(payment)
        if status_changed:
            await kafka.send_event(payment)
        await inc_api_metrics(env=ENV, method='GET', status_code=200)
        return Response(json.dumps(payment), media_type='application/json')
    except GovPayException as gpe:
        await inc_api_metrics(env=ENV, method='GET', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(gpe), None)


@app.get('/landing/{payment_id}', description='Redirect User to Landing Page', tags=['Payments'])
async def get_landing_url(payment_id: str = Path(example='5f99646d-769e-4d73-ba28-cffd30a77e74')):  # noqa: B008
    try:
        redirect_url, payment = await govpay.get_landing_url(payment_id=payment_id)
        await kafka.send_event(payment)
        await inc_api_metrics(env=ENV, method='GET', status_code=307)
        return RedirectResponse(redirect_url)
    except GovPayException as gpe:
        await inc_api_metrics(env=ENV, method='GET', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(gpe), None)


# API tenant configuration


@app.get('/tenants/schema', description='Get Tenant Form Schema', tags=['Tenants'])
async def get_tenant_form_schema():
    try:
        await inc_api_metrics(env=ENV, method='GET', status_code=200)
        return Response(get_tenant_schema(), media_type='application/json')
    except SchemaNotFound as ce:
        await inc_api_metrics(env=ENV, method='GET', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(ce), None)


@app.get('/tenants/{tenant_id}', description='Get Tenant Configuration', response_model=Tenant, tags=['Tenants'])
async def get_tenant_configuration(
    tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6')),  # noqa: B008
):
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        await inc_api_metrics(env=ENV, method='GET', status_code=200)
        return tc.get_configuration()
    except ConfigurationNotFound as ce:
        await inc_api_metrics(env=ENV, method='GET', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(ce), None)
    except ValidationError as e:
        await inc_api_metrics(env=ENV, method='GET', status_code=422)
        return error_message('/errors/unprocessable-entity', 'Unprocessable Entity', 422, str(e), None)


@app.post(
    '/tenants',
    description='Create New Tenant Configuration',
    status_code=status.HTTP_201_CREATED,
    tags=['Tenants'],
)
async def save_tenant_configuration(configuration: Tenant):
    try:
        tc = TenantConfigurator(tenant_id=configuration.id)
        tc.update_or_save_configuration(tenant=configuration, update=False)
        govpay.add_or_update_tenant_conf(configuration.to_dict())
        logger.debug(f'Updated tenant conf dict: {govpay.tenants}')
        await inc_api_metrics(env=ENV, method='POST', status_code=201)
        return Response(status_code=201, media_type='application/json')
    except ConfigurationNotFound as ce:
        await inc_api_metrics(env=ENV, method='POST', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(ce), None)
    except ConfigurationNotAllowed as e:
        await inc_api_metrics(env=ENV, method='POST', status_code=400)
        return error_message('/errors/bad-request', 'Bad Request', 400, str(e), None)


@app.put('/tenants/{tenant_id}', description='Edit Existing Tenant Configuration', tags=['Tenants'])
async def update_tenant_configuration(
    configuration: TenantUpdate,
    tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6')),  # noqa: B008
):
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        config_data = Tenant(**configuration.to_dict(), id=tenant_id)
        tc.update_or_save_configuration(tenant=config_data, update=True)
        govpay.add_or_update_tenant_conf(config_data.to_dict())
        logger.debug(f'Updated tenant conf dict: {govpay.tenants}')
        await inc_api_metrics(env=ENV, method='PUT', status_code=200)
        return Response(status_code=200, media_type='application/json')
    except ConfigurationNotFound as ce:
        await inc_api_metrics(env=ENV, method='PUT', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(ce), None)
    except ConfigurationNotAllowed as e:
        await inc_api_metrics(env=ENV, method='PUT', status_code=400)
        return error_message('/errors/bad-request', 'Bad Request', 400, str(e), None)


@app.patch('/tenants/{tenant_id}', description='Patch Existing Tenant Configuration', tags=['Tenants'])
async def update_existing_tenant_configuration(
    tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6')),  # noqa: B008
    new_configuration: dict = Body(  # noqa: B008
        example={'tax_identification_number': '1234567891'},
        media_type='application/json-patch+json',
    ),
):
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        config_data = tc.update_existing_configuration(new_configuration=new_configuration)
        govpay.add_or_update_tenant_conf(config_data)
        logger.debug(f'Updated tenant conf dict: {govpay.tenants}')
        await inc_api_metrics(env=ENV, method='PATCH', status_code=200)
        return Response(status_code=200, media_type='application/json')
    except ConfigurationNotFound as ce:
        await inc_api_metrics(env=ENV, method='PATCH', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(ce), None)
    except ConfigurationNotAllowed as e:
        await inc_api_metrics(env=ENV, method='PATCH', status_code=400)
        return error_message('/errors/bad-request', 'Bad Request', 400, str(e), None)


@app.delete(
    '/tenants/{tenant_id}',
    description='Disable Tenant Configuration',
    status_code=status.HTTP_204_NO_CONTENT,
    tags=['Tenants'],
)
async def delete_tenant(tenant_id: UUID = Path(example=UUID('b212c4b4-db26-4404-8c7c-47dab99dd2e6'))):  # noqa: B008
    try:
        tc = TenantConfigurator(tenant_id=tenant_id)
        tc.disable_configuration()
        govpay.delete_tenant_conf(tenant_id)
        logger.debug(f'Updated tenant conf dict: {govpay.tenants}')
        await inc_api_metrics(env=ENV, method='DELETE', status_code=204)
        return Response(status_code=204)
    except (ConfigurationNotFound, KeyError) as e:
        await inc_api_metrics(env=ENV, method='DELETE', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(e), None)
    except ConfigurationNotAllowed as e:
        await inc_api_metrics(env=ENV, method='DELETE', status_code=400)
        return error_message('/errors/bad-request', 'Bad Request', 400, str(e), None)


# API service configuration


@app.get('/services/schema', description='Get Service Form Schema', tags=['Services'])
async def get_service_form_schema():
    try:
        await inc_api_metrics(env=ENV, method='GET', status_code=200)
        return Response(get_service_schema(), media_type='application/json')
    except SchemaNotFound as ce:
        await inc_api_metrics(env=ENV, method='GET', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(ce), None)


@app.get('/services/{service_id}', description='Get Service Configuration', response_model=Service, tags=['Services'])
async def get_service_configuration(
    service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248'),  # noqa: B008
):
    try:
        sc = ServiceConfigurator(
            tenant_id=UUID(govpay.services[str(service_id)]['tenant_id'])
            if str(service_id) in govpay.services
            else None,
            service_id=service_id,
        )
        return sc.get_configuration()
    except ConfigurationNotFound as ce:
        await inc_api_metrics(env=ENV, method='GET', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(ce), None)
    except ValidationError as e:
        await inc_api_metrics(env=ENV, method='GET', status_code=422)
        return error_message('/errors/unprocessable-entity', 'Unprocessable Entity', 422, str(e), None)


@app.post(
    '/services',
    description='Create New Service Configuration',
    status_code=status.HTTP_201_CREATED,
    tags=['Services'],
)
async def save_service_configuration(configuration: Service):
    try:
        if not str(configuration.tenant_id) in govpay.tenants:
            raise ConfigurationNotFound(f"Tenant {configuration.tenant_id} doesn't exist")
        sc = ServiceConfigurator(tenant_id=configuration.tenant_id, service_id=configuration.id)
        sc.update_or_save_configuration(service=configuration, update=False)
        govpay.add_or_update_service_conf(configuration.to_dict())
        logger.debug(f'Updated services conf dict: {govpay.services}')
        await inc_api_metrics(env=ENV, method='POST', status_code=201)
        return Response(status_code=201, media_type='application/json')
    except ConfigurationNotFound as ce:
        await inc_api_metrics(env=ENV, method='POST', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(ce), None)
    except ConfigurationNotAllowed as e:
        await inc_api_metrics(env=ENV, method='POST', status_code=400)
        return error_message('/errors/bad-request', 'Bad Request', 400, str(e), None)


@app.put('/services/{service_id}', description='Create New Tenant Configuration', tags=['Services'])
async def update_service_configuration(
    configuration: ServiceUpdate,
    service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248'),  # noqa: B008
):
    try:
        if not str(configuration.tenant_id) in govpay.tenants:
            raise ConfigurationNotFound(f"Tenant {configuration.tenant_id} doesn't exist")
        sc = ServiceConfigurator(tenant_id=configuration.tenant_id, service_id=service_id)
        config_data = Service(**configuration.to_dict(), id=service_id)
        sc.update_or_save_configuration(service=config_data, update=True)
        govpay.add_or_update_service_conf(config_data.to_dict())
        logger.debug(f'Updated services conf dict: {govpay.services}')
        await inc_api_metrics(env=ENV, method='PUT', status_code=200)
        return Response(status_code=200, media_type='application/json')
    except ConfigurationNotFound as ce:
        await inc_api_metrics(env=ENV, method='PUT', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(ce), None)
    except ConfigurationNotAllowed as e:
        await inc_api_metrics(env=ENV, method='PUT', status_code=400)
        return error_message('/errors/bad-request', 'Bad Request', 400, str(e), None)


@app.patch('/services/{service_id}', description='Patch Existing Service Configuration', tags=['Services'])
async def update_existing_service_configuration(
    service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248'),  # noqa: B008
    new_configuration: dict = Body(media_type='application/json-patch+json'),  # noqa: B008
):
    try:
        sc = ServiceConfigurator(
            tenant_id=UUID(govpay.services[str(service_id)]['tenant_id'])
            if str(service_id) in govpay.services
            else None,
            service_id=service_id,
        )
        config_data = sc.update_existing_configuration(new_configuration=new_configuration)
        govpay.add_or_update_service_conf(config_data)
        logger.debug(f'Updated services conf dict: {govpay.services}')
        await inc_api_metrics(env=ENV, method='PATCH', status_code=200)
        return Response(status_code=200, media_type='application/json')
    except ConfigurationNotFound as ce:
        await inc_api_metrics(env=ENV, method='PATCH', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(ce), None)
    except ConfigurationNotAllowed as e:
        await inc_api_metrics(env=ENV, method='PATCH', status_code=400)
        return error_message('/errors/bad-request', 'Bad Request', 400, str(e), None)


@app.delete(
    '/services/{service_id}',
    description='Disable Service Configuration',
    status_code=status.HTTP_204_NO_CONTENT,
    tags=['Services'],
)
async def delete_service(service_id: UUID = Path(example='23d57b65-5eb9-4f0a-a507-fbcf3057b248')):  # noqa: B008
    try:
        sc = ServiceConfigurator(
            tenant_id=UUID(govpay.services[str(service_id)]['tenant_id'])
            if str(service_id) in govpay.services
            else None,
            service_id=service_id,
        )
        sc.disable_configuration()
        govpay.delete_service_conf(service_id)
        logger.debug(f'Updated service conf dict: {govpay.services}')
        await inc_api_metrics(env=ENV, method='DELETE', status_code=200)
        return Response(status_code=204)
    except (ConfigurationNotFound, KeyError) as e:
        await inc_api_metrics(env=ENV, method='DELETE', status_code=404)
        return error_message('/errors/item-not-found', 'Item not found', 404, str(e), None)
    except ConfigurationNotAllowed as e:
        await inc_api_metrics(env=ENV, method='DELETE', status_code=400)
        return error_message('/errors/bad-request', 'Bad Request', 400, str(e), None)


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title='GovPay Proxy',
        version=app_version,
        tags=[{'name': 'Status'}, {'name': 'Payments'}, {'name': 'Tenants'}, {'name': 'Services'}],
        servers=[{'url': EXTERNAL_API_URL, 'description': 'govpay-payment-proxy', 'x-sandbox': True}],
        description='Questo servizio si occupa di fare da intermediario '
        "tra Opencity Italia - Area Personale e il provider di pagamento GovPay",
        contact={
            'name': 'Opencity Labs Srl',
            'url': 'https://opencitylabs.it/collabora-con-noi/',
            'email': 'info@opencitylabs.it',
        },
        terms_of_service='http://example.com/terms/',
        routes=app.routes,
    )
    openapi_schema['info']['x-api-id'] = 'govpay-payment-proxy'
    openapi_schema['info']['x-summary'] = (
        "Questo servizio si occupa di fare da intermediario tra Opencity Italia - Area Personale e il provider di pagamento GovPay",
    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi

govpay = GovPay(
    config_path=GOVPAY_CONFIG_PATH,
    gotenberg_url=GOTENBERG_URL,
    app_online_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_online_url', payment_id='{id}')}",
    app_landing_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_landing_url', payment_id='{id}')}",
    app_notice_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_notice_file', payment_id='{id}')}",
    app_receipt_url=f"{EXTERNAL_API_URL}{app.url_path_for('get_receipt_file', payment_id='{id}')}",
    app_update_url=f"{INTERNAL_API_URL}{app.url_path_for('check_for_updates', payment_id='{id}')}",
    checkout_url=PAGOPA_PAYMENT_URL,
)

if __name__ == '__main__':
    uvicorn.run(
        app,
        host=APP_HOST,
        port=APP_PORT,
        proxy_headers=True,
        forwarded_allow_ips='*',
        access_log=False,
    )

import json
import mimetypes
import os
import tempfile

import cloudstorage
from cloudstorage import get_driver_by_name
from dotenv import load_dotenv
from minio import Minio

from logger import file_logger as logger

load_dotenv()

STORAGE_ENDPOINT = os.environ.get('STORAGE_ENDPOINT', '')
STORAGE_TYPE = os.environ.get('STORAGE_TYPE', 'LOCAL')
STORAGE_KEY = os.environ.get('STORAGE_KEY', '../')
STORAGE_SECRET = os.environ.get('STORAGE_SECRET', '')
STORAGE_BUCKET_NAME = os.environ.get('STORAGE_BUCKET_NAME', 'payments')
STORAGE_REGION = os.environ.get('STORAGE_REGION', '')


driver_cls = get_driver_by_name(STORAGE_TYPE)
storage = driver_cls(
    endpoint=STORAGE_ENDPOINT,
    key=STORAGE_KEY,
    secret=STORAGE_SECRET,
    region=STORAGE_REGION,
    secure=False if 'amazonaws' not in STORAGE_ENDPOINT or 'S3' not in STORAGE_TYPE else True,
)


class FileManager:
    def __init__(self, container_name=STORAGE_BUCKET_NAME):
        self.container_name = container_name
        try:
            self.container = storage.get_container(container_name)
        except cloudstorage.exceptions.NotFoundError:
            self.container = storage.create_container(container_name)
        except Exception as e:
            logger.error(f'An error occurred: {e}')

    def save_file(self, path, data: str):
        try:
            logger.debug('Saving object to bucket...')
            with tempfile.NamedTemporaryFile(mode='w+') as temp:
                temp.write(data)
                temp.seek(os.SEEK_SET)
                self.container.upload_blob(filename=temp.name, blob_name=path)
        except Exception as e:
            logger.error(f'An error occurred while saving object: {e}')

    def get_file(self, path):
        try:
            logger.debug(f'Getting object {path}')
            blob = self.container.get_blob(path)
            filename = blob.name.split('/')[-1]
            blob.download(filename)
            with open(filename) as f:
                obj = f.read()
            os.remove(filename)
            return obj
        except cloudstorage.exceptions.NotFoundError as nfe:
            logger.error(f'Object {path} not found in cloud: {nfe}')
        except FileNotFoundError as fnfe:
            logger.error(f'Object {path} not found: {fnfe}')
        except IOError as ioe:
            logger.error(f'An error occurred while reading obj {path}: {ioe}')
        return None

    def file_exists(self, path):
        try:
            self.container.get_blob(path)
            return True
        except cloudstorage.exceptions.NotFoundError:
            return False

    def get_all_files(self, base_path):
        try:
            if STORAGE_TYPE == 'LOCAL':
                return self._from_local(base_path)
            elif STORAGE_TYPE == 'MINIO':
                return self._from_minio(base_path)
            else:
                return self._from_remote(base_path)
        except cloudstorage.exceptions.NotFoundError as nfe:
            logger.error(f'An error occurred: {nfe}')
        except FileNotFoundError as ex:
            logger.error(f'File {base_path} missing')
            raise Exception(f'Invalid path {base_path}: {ex}')

    def _from_local(self, base_path):
        configs = {}
        file_count = 0
        for subdir, _, files in os.walk(f'{STORAGE_KEY}{self.container_name}/{base_path}'):
            for file in files:
                with open(os.path.join(subdir, file), 'rt') as config:
                    if mimetypes.MimeTypes().guess_type(file)[0] == 'application/json':
                        config = json.load(config)
                        configs[f'{file}_{file_count}'] = config
                        file_count += 1
        return configs

    def _from_minio(self, base_path):
        configs = {}
        file_count = 0
        minio_client = Minio(
            endpoint=STORAGE_ENDPOINT,
            access_key=STORAGE_KEY,
            secret_key=STORAGE_SECRET,
            secure=False,
        )
        objects = minio_client.list_objects(bucket_name=self.container_name, prefix=base_path, recursive=True)
        for obj in objects:
            logger.debug(f'Path: {obj.object_name}')
            file_path = obj.object_name.split('/')[-1]
            minio_client.fget_object(bucket_name=self.container_name, object_name=obj.object_name, file_path=file_path)
            with open(file_path) as f:
                if mimetypes.MimeTypes().guess_type(file_path)[0] == 'application/json':
                    config = json.load(f)
                    configs[f"{obj.object_name.split('/')[-1]}_{file_count}"] = config
                    file_count += 1
            os.remove(file_path)
        return configs

    def _from_remote(self, base_path):
        configs = {}
        file_count = 0
        for blob in self.container:
            if base_path in blob.path:
                logger.debug(f'Path: {blob.path}')
                path = '/'.join(blob.path.split('/')[1:])
                config = json.loads(self.get_file(path=path))
                configs[f"{blob.name.split('/')[-1]}_{file_count}"] = config
                file_count += 1
        return configs

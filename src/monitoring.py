from prometheus_client import Counter, Histogram

APP_NAME = 'govpay-payment-proxy'

API_REQUESTS_COUNT = Counter(
    'oc_api_requests_total',
    'Number of payment api responses',
    ('env', 'method', 'app_name', 'status_code'),
)

PROVIDER_LATENCY = Histogram(
    'oc_payment_provider_latency',
    'Duration of provider requests',
    ('env', 'app_name'),
    buckets=(0.1, 0.25, 0.5, 0.75, 1.0, 2.5, 5.0, 7.5, 10.0, 15.0, 20.0, 30.0),
)

SUCCESS_EVENTS_COUNT = Counter(
    'oc_payment_success_events_total',
    'Number of processed payments with success',
    ('env', 'app_name'),
)

FAILED_EVENTS_COUNT = Counter(
    'oc_payment_failed_events_total',
    'Number of processed payments with errors',
    ('env', 'app_name'),
)

VALIDATION_ERROR_PAYMENTS_COUNT = Counter(
    'oc_payment_validation_errors_total',
    'Number of processed payments with validation errors',
    ('env', 'app_name'),
)

PROVIDER_ERROR_PAYMENTS_COUNT = Counter(
    'oc_payment_provider_errors_total',
    'Number of processed payments with provider errors',
    ('env', 'app_name'),
)

INTERNAL_ERROR_PAYMENTS_COUNT = Counter(
    'oc_payment_internal_errors_total',
    'Number of processed payments with internal errors',
    ('env', 'app_name'),
)


async def inc_api_metrics(env, method, status_code):
    API_REQUESTS_COUNT.labels(env, method, APP_NAME, status_code).inc()


async def inc_success_event_metrics(env):
    SUCCESS_EVENTS_COUNT.labels(env, APP_NAME).inc()


async def inc_failed_event_metrics(env):
    FAILED_EVENTS_COUNT.labels(env, APP_NAME).inc()


async def inc_validation_error_payment_metrics(env):
    VALIDATION_ERROR_PAYMENTS_COUNT.labels(env, APP_NAME).inc()


def inc_provider_error_payment_metrics(env):
    PROVIDER_ERROR_PAYMENTS_COUNT.labels(env, APP_NAME).inc()


def inc_internal_error_payment_metrics(env):
    INTERNAL_ERROR_PAYMENTS_COUNT.labels(env, APP_NAME).inc()


def inc_provider_latency_metrics(env):
    return PROVIDER_LATENCY.labels(env, APP_NAME).time()

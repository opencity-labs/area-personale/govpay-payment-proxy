# Logger configuration
import logging
import os

from dotenv import load_dotenv

load_dotenv()

LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')
LOG_FORMAT = os.environ.get('LOG_FORMAT', '%(asctime)s %(name)-32s %(levelname)-8s %(message)s')
logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)

govpay_logger = logging.getLogger('govpay-payment-proxy.govpay')
proxy_logger = logging.getLogger('govpay-payment-proxy.app')
kafka_logger = logging.getLogger('govpay-payment-proxy.kafka')
file_logger = logging.getLogger('govpay-payment-proxy.file')
config_logger = logging.getLogger('govpay-payment-proxy.config')
ksqldb_logger = logging.getLogger('govpay-payment-proxy.ksql')

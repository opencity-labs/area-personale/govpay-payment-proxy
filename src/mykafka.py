import asyncio
import json
import os
import traceback
import uuid
from datetime import datetime
from json import JSONDecodeError
from typing import Set

from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from aiokafka.structs import TopicPartition
from pydantic import ValidationError

from file_manager import FileManager
from logger import kafka_logger as logger
from monitoring import inc_failed_event_metrics, inc_success_event_metrics, inc_validation_error_payment_metrics

BASE_PATH_EVENT = os.environ.get('BASE_PATH_EVENT', 'payments/')
EVENT_VERSION = os.environ.get('EVENT_VERSION', 2.0)
ENV = os.getenv('ENV', 'LOCAL')


class Kafka:
    def __init__(
        self,
        topic,
        bootstrap_servers,
        group_id,
        app_id,
        enable_auto_commit=True,
        seek_to_beginning=False,
    ):
        self.topic = topic
        self.bootstrap_servers = bootstrap_servers
        self.group_id = group_id
        self.enable_auto_commit = enable_auto_commit
        self.seek_to_beginning = seek_to_beginning

        self.consumer = None
        self.consumer_task = None
        self.must_stop = False

        self.app_id = app_id

    async def _init_consumer(self):
        logger.debug(
            f'Initializing KafkaConsumer for topic {self.topic}, '
            f'group_id {self.group_id} and using bootstrap servers {self.bootstrap_servers} ',
        )
        consumer = AIOKafkaConsumer(
            self.topic,
            bootstrap_servers=self.bootstrap_servers,
            group_id=self.group_id,
            enable_auto_commit=self.enable_auto_commit,
            session_timeout_ms=45000,  # Aumenta il tempo prima che Kafka lo rimuova dal gruppo
            heartbeat_interval_ms=10000  # Aumenta la frequenza degli heartbeat
        )

        # get cluster layout and join group
        await consumer.start()

        partitions: Set[TopicPartition] = consumer.assignment()

        for tp in partitions:
            # get the log_end_offset
            end_offset_dict = await consumer.end_offsets([tp])
            end_offset = end_offset_dict[tp]

            if self.seek_to_beginning:
                await consumer.seek_to_beginning(tp)
            else:
                if end_offset == 0:
                    logger.debug(
                        f'Topic ({self.topic}) has no messages (log_end_offset: {end_offset}), '
                        f'starting from the beginning ...',
                    )
                    consumer.seek(tp, 0)
                else:
                    logger.debug(f'Found log_end_offset: {end_offset} seeking to {end_offset - 1}')
                    consumer.seek(tp, end_offset - 1)
        return consumer

    async def initialize(self):
        self.consumer = await self._init_consumer()

    async def consume(self, func):
        self.consumer_task = asyncio.create_task(self.consume_message(func))

    async def consume_message(self, func, **kwargs):
        try:
            # consume messages
            logger.info('Starting consuming messages')
            async for msg in self.consumer:
                event = None
                try:
                    event = json.loads(msg.value)
                    logger.debug(event)
                    if float(event['event_version']) == EVENT_VERSION:
                        processed_event = await func(event, **kwargs)
                        if processed_event is not None:
                            if processed_event['status'] != 'CREATION_FAILED':
                                await inc_success_event_metrics(env=ENV)
                            await self.send_event(processed_event)
                        if self.must_stop:
                            logger.debug(f'Stopping consumer {self.group_id} for internal request')
                            break
                    else:
                        logger.debug(
                            f"Event {event['id']} with version {event['event_version']} is not supported, skipping it",
                        )
                except (ValidationError, TypeError, KeyError) as ex:
                    await inc_failed_event_metrics(env=ENV)
                    await inc_validation_error_payment_metrics(env=ENV)
                    logger.warning(
                        f'Processing failed: message {event} is not valid, skipping the event. Detail: {ex}',
                    )
                except JSONDecodeError as jde:
                    await inc_failed_event_metrics(env=ENV)
                    logger.warning(f'An error occurred while decoding the message: {jde}, skipping it')
                except Exception as ex:
                    await inc_failed_event_metrics(env=ENV)
                    logger.error(f"Event {event['id']} from application {event['remote_id']}: Processing failed {ex}")
            await self.consumer.stop()
        except Exception as ex:
            logger.exception(f'An error occurred while consuming messages: {ex}')
            exit(1)

    async def send_event(self, event):
        logger.debug(event)
        producer = AIOKafkaProducer(bootstrap_servers=self.bootstrap_servers)
        await producer.start()
        try:
            if 'remote_id' in event:
                logger.info(
                    f"Event {event['id']} from application {event['remote_id']}: Sending event to topic {self.topic}",
                )
            else:
                logger.info(f"Event {event['id']}: Sending event to topic  {self.topic}")
            event['event_id'] = str(uuid.uuid4())
            event['event_created_at'] = datetime.now().replace(microsecond=0).astimezone().isoformat()
            event['app_id'] = self.app_id
            event_key = event['service_id'] if 'service_id' in event else '00000000-0000-0000-0000-000000000000'
            await producer.send_and_wait(
                topic=self.topic,
                value=json.dumps(event).encode('utf-8'),
                key=event_key.encode('utf-8'),
            )
            FileManager().save_file(path=f"{BASE_PATH_EVENT}{event['id']}.json", data=json.dumps(event))
        except Exception as ex:
            logger.error(
                f"Event {event['id']} from application {event['remote_id']}: "
                f'Sending to topic  {self.topic} failed: {str(ex)}',
            )
        finally:
            # wait for all pending messages to be delivered or expire.
            logger.debug('Stopping producer')
            await producer.stop()

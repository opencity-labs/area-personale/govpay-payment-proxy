import json
import os
from abc import ABC, abstractmethod
from typing import List, Literal, Optional
from uuid import UUID

from pydantic import BaseModel, Field, constr, root_validator

from file_manager import FileManager
from logger import config_logger as logger

BASE_PATH_CONFIG = os.environ.get('BASE_PATH_CONFIG', 'tenants/')
FORM_SCHEMA_PATH = '../config'

SERVICE_PATH = BASE_PATH_CONFIG + '{tenant_id}/{service_id}.json'
SERVICE_FORM_SCHEMA_PATH = f'{FORM_SCHEMA_PATH}/service-form-schema.json'

TENANT_PATH = BASE_PATH_CONFIG + '{tenant_id}/tenant.json'
TENANT_FORM_SCHEMA_PATH = f'{FORM_SCHEMA_PATH}/tenant-form-schema.json'

DISABLE_CONFIG = {'active': False}

file_manager = FileManager()


class SchemaNotFound(Exception):  # noqa: N818
    """Used to handle when the resource is not found and return 404"""

    pass


class ConfigurationNotFound(Exception):  # noqa: N818
    """
    Used to handle when the resource is not found and return 404
    """

    pass


class ConfigurationNotAllowed(Exception):  # noqa: N818
    """
    Used to handle all known non-critical exception and return 400
    """

    pass


class ConfiguratorEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, UUID):
            return str(o)
        return o.__dict__


class Configurator(ABC):
    def json(self) -> str:
        return json.dumps(self, cls=ConfiguratorEncoder)

    def dict(self) -> dict:
        return json.loads(json.dumps(self, cls=ConfiguratorEncoder))

    @abstractmethod
    def get_configuration(self):
        pass

    @abstractmethod
    def update_or_save_configuration(self, tenant, update):
        pass

    @abstractmethod
    def update_existing_configuration(self, new_configuration):
        pass


def load_json_file(path: str) -> dict:
    """handles the loading of a json file and returns a dictionary"""
    if not file_manager.file_exists(path=path):
        logger.debug(f'Configuration not found at path: {path}')
        raise ConfigurationNotFound('Configuration not found')
    try:
        json_file = json.loads(file_manager.get_file(path=path))
    except json.JSONDecodeError as jde:
        logger.error(f'Json at path {path} not valid. Error {jde}')
        raise ConfigurationNotFound('The json in the filesystem is not valid.')
    except Exception as e:
        logger.error(f'General error at path {path}: {e}')
        raise ConfigurationNotFound('Error trying to retrieve the configuration')
    else:
        return json_file


def save_json_file(data: BaseModel, path: str, update: bool) -> dict:
    """
    saves a json file on the filesystem
    creating the path if it doesn't exist

    Parameters
    ----------
    data: BaseModel
        data is a pydantic object with json() method
    path: str
        the path to save the data as json,
        will be created if it doesn't exist
    update: bool
        if the file has to be updated or not
    """
    try:
        if not update and file_manager.file_exists(path=path) and load_json_file(path=path)['active'] is True:
            logger.debug(
                f'Path error - configuration at path {path} already exists. POST not allowed, use PUT or PATCH.',
            )
            raise ConfigurationNotAllowed('The file already exists, use method PUT or PATCH to edit the configuration')
        file_manager.save_file(path=path, data=data.json())
        return json.loads(data.json())
    except ConfigurationNotAllowed:
        raise
    except Exception as e:
        logger.error(f'Path {path} error: {str(e)}')
        raise ConfigurationNotFound('Error trying to save the resource in the filesystem, path error')


def update_configuration(path: str, new_configuration: dict) -> dict:
    """
    Updates the configuration of an existent
    tenant or service in the filesystem.

    Parameters
    ----------
    path: str
        the path for the json to update
    new_configuration: dict
        the object containing the fields to update
    """
    if 'id' in new_configuration or 'tenant_id' in new_configuration:
        raise ConfigurationNotAllowed('Edit id or tenant_id not allowed')
    return dict(load_json_file(path), **new_configuration)


# --------------------- TENANT ---------------------


class FileModel(BaseModel):
    hash: str
    name: str
    originalName: str
    size: int = Field(..., format='int32')
    storage: str
    type: str
    url: str


class TenantUpdate(BaseModel):
    tax_identification_number: str
    a2a_id: str
    auth_type: str
    username: Optional[str]
    password: Optional[str]
    key_file: Optional[List[FileModel]] = []
    cert_file: Optional[List[FileModel]] = []
    debts_api_url: str
    payments_api_url: str
    active: bool

    @root_validator()
    @classmethod
    def validate_all_fields(cls, values):
        username_exists = 'username' in values and values['username']
        password_exists = 'password' in values and values['password']
        basic_auth_exists = username_exists and password_exists
        key_file_exists = 'key_file' in values and values['key_file']
        cert_file_exists = 'cert_file' in values and values['cert_file']
        certificate_exists = key_file_exists and cert_file_exists

        if values['auth_type'] not in ['basic_auth', 'certificate']:
            raise ValueError('Payment type must be one of: basic_auth or certificate')
        if values['auth_type'] == 'basic_auth' and not basic_auth_exists:
            raise ValueError('You must fully configure either a basic authentication or a certificate authentication')
        if values['auth_type'] == 'certificate' and not certificate_exists:
            raise ValueError('You must fully configure either a basic authentication or a certificate authentication')

        return values

    class Config:
        schema_extra = {
            'example': {
                'tax_identification_number': '01234567',
                'a2a_id': 'CBugliano',
                'auth_type': 'basic_auth',
                'username': 'user',
                'password': '76JWL*G6bS!OM2e9*3Fg',
                'key_file': [
                    {
                        'hash': '11aece5092dc5e670a82c0983fd98a95',
                        'name': 'example.key',
                        'originalName': 'example.key',
                        'size': 92207,
                        'storage': 'base64',
                        'type': 'application/pdf',
                        'url': 'data:application/pdf;base64,JVBERi0xLjc',
                    },
                ],
                'cert_file': [
                    {
                        'hash': '11aece5092dc5e670a82c0983fd98a95',
                        'name': 'example.cer',
                        'originalName': 'example.cer',
                        'size': 92207,
                        'storage': 'base64',
                        'type': 'application/pdf',
                        'url': 'data:application/pdf;base64,JVBERi0xLjc',
                    },
                ],
                'debts_api_url': 'https://debts.example.com/api/v2',
                'payments_api_url': 'https://payments.example.com/api/v2',
                'active': True,
            },
        }

    def to_dict(self) -> dict:
        return json.loads(json.dumps(self, cls=ConfiguratorEncoder))


class Tenant(TenantUpdate):
    id: UUID

    class Config:
        schema_extra = {
            'example': {
                'tax_identification_number': '01234567',
                'a2a_id': 'CBugliano',
                'auth_type': 'basic_auth',
                'username': 'user',
                'password': '76JWL*G6bS!OM2e9*3Fg',
                'key_file': [],
                'cert_file': [],
                'debts_api_url': 'https://debts.example.com/api/v2',
                'payments_api_url': 'https://payments.example.com/api/v2',
                'active': True,
                'id': 'b212c4b4-db26-4404-8c7c-47dab99dd2e6',
            },
        }


class TenantConfigurator(Configurator):
    def __init__(self, tenant_id: UUID) -> None:
        self.path = TENANT_PATH.format(tenant_id=tenant_id)

    def get_configuration(self) -> Tenant:
        return Tenant.parse_obj(load_json_file(self.path))

    def update_or_save_configuration(self, tenant: Tenant, update: bool) -> dict:
        return save_json_file(data=tenant, path=self.path, update=update)

    def update_existing_configuration(self, new_configuration: dict) -> dict:
        data = Tenant.parse_obj(update_configuration(path=self.path, new_configuration=new_configuration))
        return save_json_file(data=data, path=self.path, update=True)

    def disable_configuration(self):
        data = Tenant.parse_obj(update_configuration(path=self.path, new_configuration=DISABLE_CONFIG))
        return save_json_file(data=data, path=self.path, update=True)


def get_tenant_schema() -> json:
    try:
        with open(TENANT_FORM_SCHEMA_PATH) as f:
            obj = json.load(f)
        return json.dumps(obj)
    except FileNotFoundError:
        raise SchemaNotFound('Form schema not available')


# --------------------- SERVICE ---------------------


class PaymentStamp(BaseModel):
    id: constr(max_length=2)
    amount: float = Field(..., format='float')
    reason: Optional[constr(max_length=140)]


class PaymentSplit(BaseModel):
    id: str
    budget_chapter: str
    financial_year: int = Field(..., format='int32')
    amount: float = Field(..., format='float')
    assessment: Optional[str]
    title: Optional[str]
    type: Optional[str]
    category: Optional[str]
    article: Optional[str]


class ServiceUpdate(BaseModel):
    tenant_id: UUID
    payment_type: Optional[str] = 'pagopa'
    debt_type: str = Field(pattern=r'^[a-zA-Z0-9\-_\.]{1,35}$')
    operational_unit: Optional[str] = Field(pattern=r'^[a-zA-Z0-9\-_]{1,35}$')
    taxonomy: Optional[constr(max_length=35)]
    notice_taxonomy: Optional[
        Literal[
            '',
            'Cartelle esattoriali',
            'Diritti e concessioni',
            'Imposte e tasse',
            'IMU, TASI e altre tasse comunali',
            'Ingressi a mostre e musei',
            'Multe e sanzioni amministrative',
            'Previdenza e infortuni',
            'Servizi erogati dal comune',
            'Servizi erogati da altri enti',
            'Servizi scolastici',
            'Tassa automobilistica',
            'Ticket e prestazioni sanitarie',
            'Trasporti, mobilità e parcheggi',
        ]
    ]
    management_id: Optional[constr(max_length=35)]
    division_id: Optional[constr(max_length=35)]
    split: Optional[List[PaymentSplit]] = []
    stamps: Optional[List[PaymentStamp]] = []
    active: bool

    class Config:
        schema_extra = {
            'example': {
                'tenant_id': 'b212c4b4-db26-4404-8c7c-47dab99dd2e6',
                'payment_type': 'pagopa',
                'debt_type': 'DIRITTI_SEGRETERIA',
                'operational_unit': 'UO332',
                'taxonomy': 'Sanzioni',
                'notice_taxonomy': 'Servizi scolastici',
                'management_id': 'Direzione',
                'division_id': 'Divisione',
                'split': [
                    {
                        'id': 'c_1',
                        'budget_chapter': 'Cap1',
                        'assessment': 'Acc1',
                        'title': 'Tit1',
                        'type': 'Tipo1',
                        'category': 'Cat1',
                        'article': 'Art1',
                        'financial_year': 2023,
                        'amount': 15,
                    },
                ],
                'active': True,
            },
        }

    def to_dict(self) -> dict:
        return json.loads(json.dumps(self, cls=ConfiguratorEncoder))


class Service(ServiceUpdate):
    id: UUID

    class Config:
        schema_extra = {
            'example': {
                'tenant_id': 'b212c4b4-db26-4404-8c7c-47dab99dd2e6',
                'payment_type': 'pagopa',
                'debt_type': 'DIRITTI_SEGRETERIA',
                'operational_unit': 'UO332',
                'taxonomy': 'Sanzioni',
                'notice_taxonomy': 'Servizi scolastici',
                'management_id': 'Direzione',
                'division_id': 'Divisione',
                'split': [
                    {
                        'id': 'c_1',
                        'budget_chapter': 'Cap1',
                        'assessment': 'Acc1',
                        'title': 'Tit1',
                        'type': 'Tipo1',
                        'category': 'Cat1',
                        'article': 'Art1',
                        'financial_year': 2023,
                        'amount': 15,
                    },
                ],
                'id': '23d57b65-5eb9-4f0a-a507-fbcf3057b248',
                'active': True,
            },
        }


class ServiceConfigurator(Configurator):
    def __init__(self, tenant_id: Optional[UUID], service_id: UUID) -> None:
        self.path = SERVICE_PATH.format(tenant_id=tenant_id, service_id=service_id)

    def get_configuration(self) -> Service:
        return Service.parse_obj(load_json_file(self.path))

    def update_or_save_configuration(self, service: Service, update: bool) -> dict:
        return save_json_file(data=service, path=self.path, update=update)

    def update_existing_configuration(self, new_configuration: dict) -> dict:
        data = Service.parse_obj(update_configuration(path=self.path, new_configuration=new_configuration))
        return save_json_file(data=data, path=self.path, update=True)

    def disable_configuration(self):
        data = Service.parse_obj(update_configuration(path=self.path, new_configuration=DISABLE_CONFIG))
        return save_json_file(data=data, path=self.path, update=True)


def get_service_schema() -> json:
    try:
        with open(SERVICE_FORM_SCHEMA_PATH) as f:
            obj = json.load(f)
        return json.dumps(obj)
    except FileNotFoundError:
        raise SchemaNotFound('Form schema not available')

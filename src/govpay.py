import asyncio
import base64
import json
import os
from base64 import b64decode
from datetime import datetime
from enum import Enum
from tempfile import NamedTemporaryFile

import requests
import xmltodict
from oc_python_sdk.models.payment import HttpMethodType, Payment, PaymentDataSplit, PaymentStatus
from starlette import status

from file_manager import FileManager
from logger import govpay_logger as logger
from monitoring import (
    inc_internal_error_payment_metrics,
    inc_provider_error_payment_metrics,
    inc_provider_latency_metrics,
)

BASE_PATH_CONFIG = os.environ.get('BASE_PATH_CONFIG', 'tenants/')
BASE_PATH_EVENT = os.environ.get('BASE_PATH_EVENT', 'payments/')
CANCEL_PAYMENT = True if os.environ.get('CANCEL_PAYMENT', 'false').lower() == 'true' else False
ENV = os.environ.get('ENV', 'LOCAL')


class GovPayException(Exception):  # noqa: N818
    pass


class LinkType(Enum):
    INTERNAL_LINK = 'internal'
    EXTERNAL_LINK = 'external'


class GovPayPaymentStatus(Enum):
    STATUS_ESEGUITA = 'ESEGUITA'
    STATUS_NON_ESEGUITA = 'NON_ESEGUITA'
    STATUS_ESEGUITA_PARZIALE = 'ESEGUITA_PARZIALE'
    STATUS_ANNULLATA = 'ANNULLATA'
    STATUS_SCADUTA = 'SCADUTA'
    STATUS_ANOMALA = 'ANOMALA'
    STATUS_INCASSATA = 'INCASSATA'


class PagoPaPaymentStatus(Enum):
    PAGAMENTO_ESEGUITO = '0'
    PAGAMENTO_NON_ESEGUITO = '1'
    PAGAMENTO_PARZIALMENTE_ESEGUITO = '2'
    DECORRENZA_TERMINI = '3'
    DECORRENZA_TERMINI_PARZIALE = '4'


class GovPayTipoSoggetto(Enum):
    TYPE_HUMAN = 'F'
    TYPE_LEGAL = 'G'


class GovPay:
    def __init__(
        self,
        gotenberg_url,
        app_online_url,
        app_landing_url,
        app_notice_url,
        app_receipt_url,
        app_update_url,
        config_path,
        checkout_url,
    ):
        logger.debug('Starting GOVPAY initialization')

        self.gotenberg_url = gotenberg_url

        self.file_manager = FileManager()
        self.config_path = config_path

        self.tenants = {}
        self.services = {}

        self.app_online_url = app_online_url
        self.app_notice_url = app_notice_url
        self.app_receipt_url = app_receipt_url
        self.app_update_url = app_update_url
        self.app_landing_url = app_landing_url
        self.checkout_url = checkout_url

        self._load_tenants_and_services()

        logger.debug('GOVPAY initialization completed')

    @staticmethod
    @inc_provider_latency_metrics(env=ENV)
    def perform_request(config, payload, url, method):
        if config['auth_type'] == 'basic_auth':
            if method == 'put':
                return requests.put(url=url, json=payload, auth=(config['username'], config['password']), timeout=10)
            elif method == 'post':
                return requests.post(url=url, json=payload, auth=(config['username'], config['password']), timeout=10)
            else:
                return requests.get(url=url, auth=(config['username'], config['password']))
        elif config['auth_type'] == 'certificate':
            with NamedTemporaryFile('w+', delete=False) as key, NamedTemporaryFile('w+', delete=False) as cert:
                key.write(b64decode(config['key_file'][0]['url'].split(',')[1], validate=True).decode())
                key.seek(os.SEEK_SET)
                cert.write(b64decode(config['cert_file'][0]['url'].split(',')[1], validate=True).decode())
                cert.seek(os.SEEK_SET)

                cert = (cert.name, key.name)
                if method == 'put':
                    return requests.put(url=url, json=payload, cert=cert, timeout=10)
                elif method == 'post':
                    return requests.post(url=url, json=payload, cert=cert, timeout=10)
                else:
                    return requests.get(url=url, cert=cert, timeout=10)

    @staticmethod
    def _check_errors(response):
        if 'descrizione' in response:
            inc_provider_error_payment_metrics(env=ENV)
            raise GovPayException(
                f'Codice: {response["codice"]} | '
                f'Descrizione: {response["descrizione"]} | '
                f'Dettaglio: {response["dettaglio"]}',
            )

    def _load_tenants_and_services(self):
        for key, value in self.file_manager.get_all_files(BASE_PATH_CONFIG).items():
            if 'active' not in value or value['active']:
                if 'tenant' in key:
                    self.add_or_update_tenant_conf(value)
                else:
                    self.add_or_update_service_conf(value)
        logger.debug(self.tenants)
        logger.debug(self.services)

    async def _create_payment(self, event):
        payment = Payment.parse_obj(event)
        tenant = self._get_tenant_by_identifier(str(payment.tenant_id))
        service = self._get_service_by_identifier(str(payment.service_id))
        if not tenant or not service:
            return None

        logger.info(f'Event {payment.id} from application {payment.remote_id}: Preparing payment creation request')

        payment_id = str(payment.id)
        payload = self._get_creation_request(payment)
        try:
            logger.debug(
                f'Event {payment.id} from application {payment.remote_id}: '
                f'Sending creation request with data {payload}',
            )

            response = await asyncio.to_thread(
                self.perform_request(
                    config=tenant,
                    payload=payload,
                    url=f'{tenant["debts_api_url"]}/pendenze/{tenant["a2a_id"]}/{payment.payment.iud}',
                    method='put',
                )
            )
            logger.debug(response.content)
            response = response.json()
            self._check_errors(response)

            response = await asyncio.to_thread(
                self.perform_request(
                    config=tenant,
                    payload=None,
                    url=f'{tenant["debts_api_url"]}/pendenze/{tenant["a2a_id"]}/{payment.payment.iud}',
                    method='get',
                )
            )
            logger.debug(response.content)
            response = response.json()
            self._check_errors(response)

            # Aggiornamento informazioni pagamento restituite dal ws
            payment.payment.iuv = response['iuvAvviso']
            payment.payment.notice_code = response['numeroAvviso']

            # Link pagamento online
            payment.links.online_payment_begin.url = self.get_online_url(payment_id, url_type=LinkType.INTERNAL_LINK)
            payment.links.online_payment_begin.method = HttpMethodType.HTTP_METHOD_GET

            # Link per scaricare l'avviso di pagamento pdf (Pagamento offline)  # noqa: W505
            payment.links.offline_payment.url = self._get_notice_url(payment_id=payment_id)
            payment.links.offline_payment.method = HttpMethodType.HTTP_METHOD_GET

            # Link per scaricare la ricevuta di pagamento disponibile SOLO a pagamento effettuato  # noqa: W505
            payment.links.receipt.url = self._get_receipt_url(payment_id=payment_id)
            payment.links.receipt.method = HttpMethodType.HTTP_METHOD_GET

            # Link per richiedere l'aggiornamento dello stato del pagamento  # noqa: W505
            payment.links.update.url = self._get_update_url(payment_id=payment_id)
            payment.links.update.method = HttpMethodType.HTTP_METHOD_GET

            payment.status = PaymentStatus.STATUS_PAYMENT_PENDING.value
            payment = payment.update_time('links.update.next_check_at')
        except Exception as ex:
            if ex.__class__.__name__ != 'GovPayException':
                inc_internal_error_payment_metrics(env=ENV)
            payment.status = PaymentStatus.STATUS_CREATION_FAILED
            logger.error(f'Event {payment.id} from application {payment.remote_id}: Error: {ex}')

        payment = payment.update_time('updated_at')

        return json.loads(payment.json())

    async def process_payment(self, event, **kwargs):
        payment = Payment(**event)
        if self._is_external(payment):
            logger.info(f"Event {event['id']} from external source: Processing event")
            payment.links.update.url = self._get_update_url(payment_id=str(payment.id))
            payment.links.update.method = HttpMethodType.HTTP_METHOD_GET
            payment = payment.update_time('links.update.next_check_at')
            payment.links.offline_payment.url = None
            payment.links.offline_payment.method = None
            self.file_manager.save_file(path=f'{BASE_PATH_EVENT}{str(payment.id)}.json', data=json.dumps(event))
            return json.loads(payment.json())
        if payment.is_payment_creation_needed():
            logger.debug(f"Event {event['id']} from application {event['remote_id']}: Processing event")
            return await self._create_payment(event)
        else:
            logger.debug(f"Event {event['id']} from application {event['remote_id']}: Processing not required")
            return None

    def _is_external(self, payment):
        """
        :param payment:
        :return: bool
        Checks if a payment was processed by the proxy
        or if it was imported from an external source
        """
        try:
            if payment.status == PaymentStatus.STATUS_PAYMENT_PENDING and not self._payment_exists(str(payment.id)):
                self._get_service_by_identifier(service_id=str(payment.service_id))
                return True
        except GovPayException:
            return False
        return False

    async def get_online_url(self, payment_id: str, url_type: LinkType = LinkType.EXTERNAL_LINK):
        if url_type == LinkType.INTERNAL_LINK:
            return self.app_online_url.replace('{id}', payment_id)

        payment, tenant = self._get_payment_and_tenant(payment_id=payment_id)
        if tenant.get('tax_identification_number', None) and payment.payment.iuv:
            return await self._get_online_url_with_checkout(payment, tenant)
        return await self._get_online_url(payment, tenant)

    async def _get_online_url(self, payment: Payment, tenant):
        domain_id = tenant['tax_identification_number']
        notice_code = payment.payment.notice_code
        response = await asyncio.to_thread(
            self.perform_request(
                config=tenant,
                payload=None,
                url=f'{tenant["debts_api_url"]}/pendenze/byAvviso/{domain_id}/{notice_code}',
                method='get',
            )
        )
        response = response.json()
        self._check_errors(response)
        if response['stato'] == GovPayPaymentStatus.STATUS_NON_ESEGUITA.value:
            try:
                payload = await self._get_online_url_request(payment, tenant)
                logger.debug(
                    f'Event {payment.id} from application {payment.remote_id}: '
                    f'Resending creation request with data {payload}',
                )
                response = await asyncio.to_thread(
                    self.perform_request(
                        config=tenant,
                        payload=payload,
                        url=f'{tenant["payments_api_url"]}/pagamenti',
                        method='post',
                    )
                )
                response = response.json()
                self._check_errors(response)
            except Exception as ex:
                if ex.__class__.__name__ != 'GovPayException':
                    inc_internal_error_payment_metrics(env=ENV)
                logger.error(f'Event {payment.id} from application {payment.remote_id}: Error: {ex}')
                raise GovPayException(f'An error occurred while getting online url for payment {payment.id}')
        else:
            logger.error(
                f'An error occurred while getting online url for payment {payment.id}: '
                f'payment has already processed',
            )
            raise GovPayException
        payment = payment.update_time('links.online_payment_begin.last_opened_at')
        payment = payment.update_time('updated_at')
        return response['redirect'], json.loads(payment.json())

    async def _get_online_url_with_checkout(self, payment: Payment, tenant):
        payload = await self._pagopa_checkout_payload(payment, tenant)

        logger.debug(
            f'Event {payment.id} from application {payment.remote_id}: Sending creation request with data {payload}',
        )
        response = requests.post(url=self.checkout_url, json=payload, allow_redirects=False)
        if response.status_code != status.HTTP_302_FOUND:
            error = response.json()
            logger.error(f'An error occurred: {error["title"]}: {error["detail"]}')
            raise GovPayException(f'An error occurred: {error["title"]}: {error["detail"]}')

        payment = payment.update_time('links.online_payment_begin.last_opened_at')
        payment = payment.update_time('updated_at')
        return response.headers['Location'], json.loads(payment.json())

    async def get_landing_url(self, payment_id: str, url_type: LinkType = LinkType.EXTERNAL_LINK):
        if url_type == LinkType.INTERNAL_LINK:
            return self.app_landing_url.replace('{id}', payment_id)
        payment, tenant = self._get_payment_and_tenant(payment_id=payment_id)

        domain_id = tenant['tax_identification_number']
        notice_code = payment.payment.notice_code
        response = await asyncio.to_thread(
            self.perform_request(
                config=tenant,
                payload=None,
                url=f'{tenant["debts_api_url"]}/pendenze/byAvviso/{domain_id}/{notice_code}',
                method='get',
            )
        )
        response = response.json()
        self._check_errors(response)

        if response['stato'] != GovPayPaymentStatus.STATUS_NON_ESEGUITA.value:
            payment.status = PaymentStatus.STATUS_PAYMENT_STARTED

        payment = payment.update_time('links.online_payment_landing.last_opened_at')
        payment = payment.update_time('updated_at')
        return payment.links.online_payment_landing.url, json.loads(payment.json())

    def _get_update_url(self, payment_id: str):
        return self.app_update_url.replace('{id}', payment_id)

    async def check_for_updates(self, payment_id: str):
        payment, tenant = self._get_payment_and_tenant(payment_id=payment_id)

        if payment.status == PaymentStatus.STATUS_COMPLETE:
            logger.debug(f'Payment {payment.id} already completed')
            return json.loads(payment.json()), False

        try:
            logger.debug(f'Payment {payment.id}: Sending search request')
            domain_id = tenant['tax_identification_number']
            notice_code = payment.payment.notice_code
            response = await asyncio.to_thread(
                self.perform_request(
                    config=tenant,
                    payload=None,
                    url=f'{tenant["debts_api_url"]}/pendenze/byAvviso/{domain_id}/{notice_code}',
                    method='get',
                )
            )
            response = response.json()
            self._check_errors(response)
            if response['stato'] in [
                GovPayPaymentStatus.STATUS_ESEGUITA.value,
                GovPayPaymentStatus.STATUS_INCASSATA.value,
            ]:
                logger.info(
                    f'Payment {payment.payment.iuv} from application {payment.remote_id}: '
                    f'Callback with status {response["stato"]}',
                )
                payment.payment.transaction_id = response['rpp'][0]['rt']['datiPagamento']['datiSingoloPagamento'][0][
                    'identificativoUnivocoRiscossione'
                ]
                payment.payment.paid_at = response['rpp'][0]['rt']['datiPagamento']['datiSingoloPagamento'][0][
                    'dataEsitoSingoloPagamento'
                ]
                payment.status = PaymentStatus.STATUS_COMPLETE
            elif response['stato'] == GovPayPaymentStatus.STATUS_ESEGUITA_PARZIALE.value:
                payment.status = PaymentStatus.STATUS_PAYMENT_STARTED
                logger.debug(f'Payment {payment.id} has not been paid yet')
            elif response['stato'] == GovPayPaymentStatus.STATUS_SCADUTA.value:
                logger.debug(f'Payment {payment.id} has expired')
                payment.status = PaymentStatus.STATUS_EXPIRED
            else:
                logger.debug(f'Payment {payment.id} has not been paid yet')
            payment = payment.update_time('links.update.last_check_at')
            payment = payment.update_time('updated_at')
            payment = payment.update_check_time()
            return json.loads(payment.json()), True
        except Exception as ex:
            if ex.__class__.__name__ != 'GovPayException':
                inc_internal_error_payment_metrics(env=ENV)
            logger.error(f'Payment {payment_id} Error: {ex}')
            raise GovPayException(f'An error occurred while trying to update payment {payment_id}')

    def _get_notice_url(self, payment_id: str):
        return self.app_notice_url.replace('{id}', payment_id)

    async def get_notice_file(self, payment_id: str):
        payment, tenant = self._get_payment_and_tenant(payment_id=payment_id)
        payload = self._get_creation_request(payment)
        try:
            logger.debug(
                f'Payment {payment.payment.iuv} from application {payment.remote_id}: '
                f'Sending offline payment request with data {payload}',
            )

            payment = payment.update_time('links.offline_payment.last_opened_at')
            payment = payment.update_time('updated_at')

            response = await asyncio.to_thread(
                self.perform_request(
                    config=tenant,
                    payload=payload,
                    url=f'{tenant["debts_api_url"]}/pendenze/{tenant["a2a_id"]}/{payment.payment.iud}?stampaAvviso=true',
                    method='put',
                )
            )
            response = response.json()
            self._check_errors(response)
            return base64.b64decode(response['pdf']), json.loads(payment.json())
        except Exception as ex:
            if ex.__class__.__name__ != 'GovPayException':
                inc_internal_error_payment_metrics(env=ENV)
            logger.error(f'Payment {payment.id} from application {payment.remote_id}: Error: {ex}')
            raise GovPayException

    def _get_receipt_url(self, payment_id: str):
        return self.app_receipt_url.format(id=payment_id)

    async def get_receipt_file(self, payment_id: str):
        payment, tenant = self._get_payment_and_tenant(payment_id=payment_id)
        service = self._get_service_by_identifier(str(payment.service_id))
        try:
            logger.debug(
                f'Payment {payment.payment.iuv} from application {payment.remote_id}: Sending receipt request',
            )

            payment = payment.update_time('links.receipt.last_opened_at')
            payment = payment.update_time('updated_at')

            domain_id = tenant['tax_identification_number']
            notice_code = payment.payment.notice_code
            response = await asyncio.to_thread(
                self.perform_request(
                    config=tenant,
                    payload=None,
                    url=f'{tenant["debts_api_url"]}/pendenze/byAvviso/{domain_id}/{notice_code}',
                    method='get',
                )
            )
            response = response.json()
            self._check_errors(response)

            iuv = payment.payment.iuv
            ccp = response['rpp'][0]['rt']['datiPagamento']['CodiceContestoPagamento']
            response = await asyncio.to_thread(
                self.perform_request(
                    config=tenant,
                    payload=None,
                    url=f'{tenant["payments_api_url"]}/rpp/{domain_id}/{iuv}/{ccp}/rt',
                    method='get',
                )
            )
            if response.headers['Content-Type'] == 'application/json':
                response = response.json()
                self._check_errors(response)
            elif response.headers['Content-Type'] == 'text/xml':
                receipt = xmltodict.parse(
                    response.content.decode(),
                    process_namespaces=True,
                    namespaces={'http://www.digitpa.gov.it/schemas/2011/Pagamenti/': None},
                )
                template = self._get_receipt_template(receipt, service)
                receipt_pdf = self._gotenberg_request(template)
            else:
                receipt_pdf = response.content.decode()
            return receipt_pdf, json.loads(payment.json())
        except Exception as ex:
            if ex.__class__.__name__ != 'GovPayException':
                inc_internal_error_payment_metrics(env=ENV)
            logger.error(f'An error occurred while getting receipt file for payment {payment.id}: Error: {ex}')
            raise GovPayException('Receipt not available')

    @staticmethod
    def _get_receipt_template(receipt, service):
        with open('../templates/receipt_template.html') as f:
            template = f.read()

        receipt = receipt['RT']
        balance = receipt['datiPagamento']['datiSingoloPagamento']
        template = template.format(
            descrizioneServizio=service['debt_type'].replace('_', ' '),
            denominazioneBeneficiario=receipt['enteBeneficiario']['denominazioneBeneficiario'],
            codiceIdentificativoUnivocoBeneficiario=receipt['enteBeneficiario']['identificativoUnivocoBeneficiario'][
                'codiceIdentificativoUnivoco'
            ],
            importoTotalePagato=receipt['datiPagamento']['importoTotalePagato'],
            identificativoUnivocoVersamento=receipt['datiPagamento']['identificativoUnivocoVersamento'],
            anagraficaPagatore=receipt['soggettoPagatore']['anagraficaPagatore'],
            codiceIdentificativoUnivocoSoggettoVersante=receipt['soggettoPagatore']['identificativoUnivocoPagatore'][
                'codiceIdentificativoUnivoco'
            ],
            identificativoUnivocoRiscossione=balance['identificativoUnivocoRiscossione']
            if isinstance(balance, dict)
            else ', '.join([item['identificativoUnivocoRiscossione'] for item in balance]),
            dataOraMessaggioRicevuta=receipt['dataOraMessaggioRicevuta'].replace('T', ' '),
            codiceEsitoPagamento=PagoPaPaymentStatus(receipt['datiPagamento']['codiceEsitoPagamento']).name.replace(
                '_',
                ' ',
            ),
        )

        return template

    def _gotenberg_request(self, content):
        """
        Method for handling possible errors that
        may be encountered in Gotenberg request
        :param content: content to be generated using Gotenberg
        :return: Gotenberg response content
        """

        try:
            response = requests.post(
                url=self.gotenberg_url,
                files={'file': ('index.html', content)},
                data={
                    'marginTop': 0,
                    'marginLeft': 0,
                    'marginBottom': 0,
                    'marginRight': 0,
                },
            )
            return response.content
        except requests.exceptions.ConnectionError:
            logger.error(f'An error occurred: unable to connect to url {self.gotenberg_url}')
        except requests.exceptions.ReadTimeout:
            logger.error(f'An error occurred: unable to read from url {self.gotenberg_url}')
        except Exception as ex:
            logger.error(f'An error occurred from url {self.gotenberg_url}: {str(ex)}')
        raise GovPayException('An error occurred: cannot generate PDF document')

    def _get_creation_request(self, payment: Payment):
        logger.debug(f'Event {payment.id} from application {payment.remote_id}: Prepare payload for creation request')

        tenant = self._get_tenant_by_identifier(str(payment.tenant_id))
        service = self._get_service_by_identifier(str(payment.service_id))

        request_data = {
            'idTipoPendenza': service['debt_type'],
            'idDominio': tenant['tax_identification_number'],
            'idUnitaOperativa': service['operational_unit'] or None,
            'causale': payment.reason,
            'soggettoPagatore': {
                'tipo': GovPayTipoSoggetto.TYPE_HUMAN.value
                if payment.payer.type == GovPayTipoSoggetto.TYPE_HUMAN.value
                else GovPayTipoSoggetto.TYPE_LEGAL.value,
                'identificativo': payment.payer.tax_identification_number,
                'anagrafica': f'{payment.payer.name} {payment.payer.family_name}',
                'indirizzo': payment.payer.street_name or None,
                'civico': payment.payer.building_number or None,
                'cap': int(payment.payer.postal_code) if payment.payer.postal_code else None,
                'localita': payment.payer.town_name or None,
                'provincia': payment.payer.country_subdivision or None,
                'nazione': payment.payer.country or None,
                'email': payment.payer.email or None,
                'cellulare': None,
            },
            'importo': payment.payment.amount,
            'numeroAvviso': payment.payment.notice_code,
            'tassonomia': service['taxonomy'] or None,
            'tassonomiaAvviso': service['notice_taxonomy'] or None,
            'direzione': service['management_id'] or None,
            'divisione': service['division_id'] or None,
            'dataValidita': datetime.now().strftime('%Y-%m-%d'),
            'dataScadenza': payment.payment.expire_at.strftime('%Y-%m-%d'),
            'annoRiferimento': datetime.now().strftime('%Y'),
            'cartellaPagamento': payment.payment.iud,
            'datiAllegati': {},
            'documento': None,
            'dataNotificaAvviso': None,
            'dataPromemoriaScadenza': None,
            'proprieta': {},
            'voci': [
                {
                    'codEntrata': service['debt_type'],
                    'idVocePendenza': payment.payment.iud,
                    'importo': payment.payment.amount,
                    'descrizione': payment.reason,
                    'datiAllegati': {},
                    'descrizioneCausaleRPT': payment.reason,
                    'contabilita': self._prepare_split(payment, service),
                    'idDominio': tenant['tax_identification_number'],
                },
            ],
            'allegati': [],
        }

        return request_data

    def _prepare_split(self, payment: Payment, service):
        payment_split = []
        if not payment.payment.iuv:
            if not payment.payment.split:
                for item in service['split']:
                    payment.payment.split.append(
                        PaymentDataSplit(
                            code=item['id'],
                            amount=item['amount'],
                            meta={key: value for key, value in item.items() if key not in ['id', 'amount']},
                        ),
                    )
                    payment_split.append(self._split_item(item, item['amount']))
            else:
                for index, item in enumerate(service['split']):
                    for split_item in payment.payment.split:
                        if split_item.code == item['id'] and split_item.amount:
                            payment.payment.split[index].meta = {
                                key: value for key, value in item.items() if key not in ['id', 'amount']
                            }
                            payment_split.append(self._split_item(item, split_item.amount))
        else:
            for item in payment.payment.split:
                payment_split.append(self._split_item(item.meta, item.amount))
        return {'quote': payment_split, 'proprietaCustom': {}} if payment_split else {}

    @staticmethod
    def _split_item(item, amount):
        return {
            'capitolo': item['budget_chapter'],
            'annoEsercizio': item['financial_year'],
            'importo': amount,
            'accertamento': item['assessment'] or None,
            'proprietaCustom': {},
            'titolo': item['title'] or None,
            'tipologia': item['type'] or None,
            'categoria': item['category'] or None,
            'articolo': item['article'] or None,
        }

    async def _get_online_url_request(self, payment, tenant):
        logger.debug(
            f'Payment {payment.payment.iuv} from application {payment.remote_id}: '
            f'Prepare payload for online url request',
        )
        return {
            'pendenze': [
                {
                    'idA2A': tenant['a2a_id'],
                    'idPendenza': payment.payment.iud,
                },
            ],
            'urlRitorno': await self.get_landing_url(payment_id=str(payment.id), url_type=LinkType.INTERNAL_LINK),
        }

    async def _pagopa_checkout_payload(self, payment: Payment, tenant):
        logger.debug(f'Event {payment.id} from application {payment.remote_id}: Prepare payload for creation request')

        landing_url = await self.get_landing_url(payment_id=str(payment.id), url_type=LinkType.INTERNAL_LINK)

        service = self._get_service_by_identifier(str(payment.service_id))

        payload = {
            'emailNotice': payment.payer.email,
            'paymentNotices': [
                {
                    'noticeNumber': payment.payment.notice_code,
                    'fiscalCode': tenant['tax_identification_number'],
                    'amount': payment.payment.amount * 100 or service['items'][0]['amount'],
                    'companyName': f"Comune di {tenant['a2a_id'][1].upper()}{tenant['a2a_id'][2:].lower()}" if len(tenant['a2a_id']) > 2 else '',
                    'description': payment.reason or service['items'][0]['reason'],
                },
            ],
            'returnUrls': {
                'returnOkUrl': f'{landing_url}?esito=OK',
                'returnCancelUrl': f'{landing_url}?esito=KO',
                'returnErrorUrl': f'{landing_url}?esito=KO',
            },
        }

        return payload

    def _get_tenant_by_identifier(self, tenant_id: str):
        if tenant_id in self.tenants:
            return self.tenants[tenant_id]
        logger.debug(f'Missing configuration for tenant identifier {tenant_id}')
        # raise GovPayException('Tenant configuration missing')

    def _get_service_by_identifier(self, service_id: str):
        if service_id in self.services:
            return self.services[service_id]
        logger.debug(f'Missing configuration for service identifier {service_id}')
        # raise GovPayException('Service configuration missing')

    def _get_payment_and_tenant(self, payment_id: str, cancel_payment=False):
        payment = self.file_manager.get_file(path=f'{BASE_PATH_EVENT}{payment_id}.json')
        if not payment:
            raise GovPayException(f'Missing payment {payment_id}')
        payment = Payment(**json.loads(payment))
        tenant = self._get_tenant_by_identifier(str(payment.tenant_id))
        return payment, tenant

    def add_or_update_service_conf(self, config):
        self.services[config['id']] = config

    def add_or_update_tenant_conf(self, config):
        self.tenants[config['id']] = config

    def delete_tenant_conf(self, tenant_id):
        del self.tenants[str(tenant_id)]

    def delete_service_conf(self, service_id):
        del self.services[str(service_id)]

    def _payment_exists(self, payment_id):
        return self.file_manager.file_exists(path=f'{BASE_PATH_EVENT}{payment_id}.json')

from typing import Optional, Union

from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field

ERR_422_SCHEMA = {
    'description': 'Validation Error',
    'content': {
        'application/problem+json': {
            'schema': {
                '$ref': '#/components/schemas/ErrorMessage',
            },
        },
    },
}


class ErrorMessage(BaseModel):
    type: str
    title: str
    status: Optional[int] = Field(..., format='int32')
    detail: Union[str, dict]
    instance: Optional[str]


class JSONResponseCustom(JSONResponse):
    media_type = 'application/problem+json'


def error_message(err_type: str, title: str, status: Optional[int], detail, instance: Optional[str]) -> JSONResponse:
    return JSONResponse(
        content=ErrorMessage(type=err_type, title=title, status=status, detail=detail, instance=instance).__dict__,
        status_code=status,
    )

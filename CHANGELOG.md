# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Generated by [`auto-changelog`](https://github.com/CookPete/auto-changelog).

## [1.1.9](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.8...1.1.9)

### Merged

- made api calls async to avoid crash on timeout and removed useless logs [`#17`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/merge_requests/17)

## [1.1.8](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.7...1.1.8) - 2025-01-31

### Merged

- Resolve "Stato del pagamento non consistente per il cittadino" [`#15`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/merge_requests/15)

### Fixed

- Merge branch '10-stato-del-pagamento-non-consistente-per-il-cittadino' into 'main' [`#10`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/issues/10)

### Commits

- Release 1.1.8 [`fa83a68`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/fa83a6839c481f637c06338806315a1a38f91200)

## [1.1.7](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.6...1.1.7) - 2024-12-17

### Commits

- fixed online payment and added checkout to it [`0ed6a6d`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/0ed6a6d88177f027d678ca8aca5875a0565ff810)
- Release 1.1.7 [`79a744e`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/79a744e5afe413291568c596fbd750c1f2586606)

## [1.1.6](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.5...1.1.6) - 2024-12-09

### Merged

- Resolve "Lo stato dei dovuti importati tramite csv non viene aggiornato" [`#14`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/merge_requests/14)

### Fixed

- Merge branch '9-lo-stato-dei-dovuti-importati-tramite-csv-non-viene-aggiornato' into 'main' [`#9`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/issues/9)

### Commits

- Release 1.1.6 [`002e450`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/002e4501cffb2c013b17574c04916d48868055a9)

## [1.1.5](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.4...1.1.5) - 2024-09-12

### Commits

- Release 1.1.5 [`8793de2`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/8793de2d374e6fb2e5814aab04ff71a63b3b7005)
- fixed error in success metrics init [`e1ed691`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/e1ed691d15dfc75b0332c01104d997dde1538d38)

## [1.1.4](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.3...1.1.4) - 2024-09-11

### Commits

- Update .gitlab-ci.yml file [`608600e`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/608600eca42d77560c2114fd21be0193854d1610)
- Release 1.1.4 [`b976605`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/b976605a1375123206d81bf8ac68ef5ef8f66f57)

## [1.1.3](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.3-rc.0...1.1.3) - 2024-09-11

### Commits

- Release 1.1.3 [`ea2f2ff`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/ea2f2ffe5849ab104267d54bd672f2bd6a02a974)

## [1.1.3-rc.0](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.2...1.1.3-rc.0) - 2024-09-09

### Merged

- Resolve "Supportare il valore nullo sul campo Tassonomia Avviso" [`#11`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/merge_requests/11)

### Fixed

- Merge branch '7-supportare-il-valore-nullo-sul-campo-tassonomia-avviso' into 'main' [`#7`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/issues/7)

### Commits

- Release 1.1.3-rc.0 [`9596960`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/95969603aaee31bd930f8f89e1fd82ba9d4ec066)

## [1.1.2](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.1...1.1.2) - 2024-04-08

### Merged

- Resolve "Correzione metrica in caso di pagamento processato con errore" [`#5`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/merge_requests/5)

### Fixed

- Merge branch '5-correzione-metrica-in-caso-di-pagamento-processato-con-errore' into 'main' [`#5`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/issues/5)

### Commits

- Release 1.1.2 [`2bc4d0b`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/2bc4d0ba005843053835793a43ff0dc81f574b8b)

## [1.1.1](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.0...1.1.1) - 2024-02-06

### Commits

- Release 1.1.1 [`8f65f02`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/8f65f023e95e86358a0c0922c86efb8173e890fb)
- fixed missing labels in internal error metric [`45f7a0f`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/45f7a0f19c92815bd6efac874a71d23233125585)

## [1.1.0](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.1.0-rc.0...1.1.0) - 2024-01-31

### Commits

- Release 1.1.0 [`990f450`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/990f450fca8e55bf8b1d9aafe833bccdb940044e)
- fixed __version__ param [`c2c9e7b`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/c2c9e7b9dfa1b97cfd6a69aa38d4ae337551e9a7)

## [1.1.0-rc.0](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0...1.1.0-rc.0) - 2024-01-26

### Merged

- Add metrics [`#4`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/merge_requests/4)
- Fix Openapi Doc [`#3`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/merge_requests/3)

### Commits

- Release 1.1.0-rc.0 [`fc748e5`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/fc748e5155e59b419595edc07745466b41e7d1fe)
- minor fix [`b1c319a`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/b1c319a40ac4a1a38628c600a017c8c4edfe88f5)

## [1.0.0](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.10...1.0.0) - 2024-01-17

### Commits

- Release 1.0.0 [`f49380f`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/f49380f4449c1635a06e27b290f678ae1c65ae8d)

## [1.0.0-rc.10](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.9...1.0.0-rc.10) - 2024-01-16

### Commits

- Release 1.0.0-rc.10 [`d116f12`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/d116f12264efb3b9b3cf8f7e6463b6095954142e)
- added check on amount if split has null amount [`a8f25bc`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/a8f25bc6ada2dfeaa39e45f9b6912b35b1373ad3)

## [1.0.0-rc.9](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.8...1.0.0-rc.9) - 2024-01-16

### Commits

- Release 1.0.0-rc.9 [`12ff919`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/12ff919050fcc29bbe14fff164e7b9fffc01501f)
- other fixes on split and log levels [`791eade`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/791eade972d3eccf5d38cd02df01b4116cab9772)

## [1.0.0-rc.8](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.7...1.0.0-rc.8) - 2024-01-16

### Commits

- Release 1.0.0-rc.8 [`b300d5f`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/b300d5f284cfc13c501d6b5e4b15c210c846164d)
- other fixes on payment path [`dc94e47`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/dc94e47c5d3bc5d991a31dae51883bf6b722a15e)

## [1.0.0-rc.7](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.6...1.0.0-rc.7) - 2024-01-16

### Commits

- Release 1.0.0-rc.7 [`6014f32`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/6014f3284c6e61c6d31a5bd99638b718e4b75471)
- other fixes on cer format validation [`a7818be`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/a7818bee97cf8fc6c34cf3e29d9a5843d923fa28)

## [1.0.0-rc.6](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.5...1.0.0-rc.6) - 2024-01-16

### Commits

- Release 1.0.0-rc.6 [`d5237fe`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/d5237fe637d8a255726cc713c016af9299074b07)
- other fixes on split [`750c572`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/750c5724d4c90df0ba2f20a4a4bea825a9b6c40e)

## [1.0.0-rc.5](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.4...1.0.0-rc.5) - 2024-01-16

### Commits

- other fixes on address [`23a1f1a`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/23a1f1ac97b7098659c24be0e79f1f927b50f5fd)
- Release 1.0.0-rc.5 [`5f8f5d4`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/5f8f5d4c392f762aabbf739acecfa613e9972c85)

## [1.0.0-rc.4](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.3...1.0.0-rc.4) - 2024-01-16

### Commits

- fixed empty string value error on optional fields [`c4bafe7`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/c4bafe70e6a570a99f043c1cf8c98e8acc5cb741)
- Release 1.0.0-rc.4 [`e4b2147`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/e4b214773355c4fb6a7b8cacbcf11f6f219553e8)

## [1.0.0-rc.3](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.2...1.0.0-rc.3) - 2024-01-15

### Commits

- fixed hurl tests [`c135b40`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/c135b40e91d9561fd7d521ec8c2af4956f8b78cb)
- Release 1.0.0-rc.3 [`ccd5913`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/ccd5913ed1d8e8e877bf08bc07ab56c1d6c4d278)

## [1.0.0-rc.2](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.1...1.0.0-rc.2) - 2024-01-15

### Commits

- fixed dockerfile [`acf6db8`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/acf6db826ea9f030ff7270225da285a17baf4c1e)
- Release 1.0.0-rc.2 [`7d1254c`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/7d1254c0063743ef9bfa894df1ccc8a45ad37072)
- fixed dockerfile [`9cd26ca`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/9cd26ca2241b74a13fc8712cab52a6931aa83350)

## [1.0.0-rc.1](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/compare/1.0.0-rc.0...1.0.0-rc.1) - 2024-01-15

### Commits

- simplified local storage paths [`02754f8`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/02754f8c299ca5044787ccb95776fedde493c310)
- Release 1.0.0-rc.1 [`77e9457`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/77e9457999a7e2b444524c7d0893f2543a6b23e6)

## 1.0.0-rc.0 - 2024-01-15

### Merged

- Add hurl tests [`#2`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/merge_requests/2)
- Resolve "Implementazione GovPay" [`#1`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/merge_requests/1)

### Fixed

- Merge branch '2-implementazione-govpay' into 'main' [`#2`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/issues/2)
- Merge branch '2-implementazione-govpay' into 'main' [`#2`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/issues/2)

### Commits

- Initial commit [`1ed58d3`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/1ed58d3e3698d277487715323e26cf51594d6c20)
- added changelog file [`b4100c0`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/b4100c0657b1fd5ecf329c57e3de24396ca246b9)
- Release 1.0.0-rc.0 [`fa3d168`](https://gitlab.com/opencity-labs/area-personale/govpay-payment-proxy/commit/fa3d16888e887b22e70ad851f6dd39dce7c4ae84)

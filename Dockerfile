FROM tiangolo/uvicorn-gunicorn:python3.11

ARG USERNAME=app
ARG USER_GID=1000
ARG USER_UID=$USER_GID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

RUN apt-get update && apt-get install -y libmagic-dev wait-for-it

ENV TZ=Europe/Rome

# Install pip requirements
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt &&  \
    pip install --no-cache-dir cloudstorage[amazon] &&  \
    pip install --no-cache-dir cloudstorage[minio] && \
    pip install --no-cache-dir cloudstorage[local]

COPY . /app

RUN chown -R $USERNAME:$USERNAME /app
RUN chmod +x /app/src/*.py
RUN chmod +x /app/*.sh

USER $USERNAME

WORKDIR /app/src

ENTRYPOINT [ "/app/docker-entrypoint.sh" ]
CMD [ "/app/src/app.py" ]

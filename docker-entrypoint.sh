#!/usr/bin/env bash

set -eo pipefail

[[ $DEBUG ]] && set -x

if [[ $KAFKA_BOOTSTRAP_SERVERS ]]; then
  # potrebbe contenere una lista separata da virgole di servers
  # ne prendo uno solo
  kafka_host=$(echo $KAFKA_BOOTSTRAP_SERVERS | cut -d, -f1)
	wait-for-it $kafka_host -s -t 300
fi

exec $*
